#include "Vectori.h"
#include <iostream>
using namespace std;

void Afisare(int a[], int n)
{
	for (int i = 0; i < n; i++) {
		cout << a[i] << " ";
	}
	cout << endl;
}

void Linie()
{
	cout << "----------------------------------\n";
}

void GenerareCrescator(int a[], int &n)
{
	n = 5 + rand() % 5;
	for (int i = 0; i < n; i++) {
		a[i] = i*10;
	}
}

void GenereazaRandom(int a[], int &n)
{	
	//n = 5 + rand() % 5;
	for (int i = 0; i < n; i++) {
		a[i] = rand()%100;
	}
}

void GenerareRandomCrescatorRecursiv(int a[], int &n)
{	
	n = 5 + rand() % 5;
	a[0] = rand()%10;
	for (int i = 1; i < n; i++) {
		a[i] = a[i - 1] + 1 + rand()%10;
	}
}

void GenerareCrescatorRecursiv(int a[], int &n)
{
	n = 5 + rand() % 5;
	a[0] = 0;
	for (int i = 1; i < n; i++) {
		a[i] = a[i - 1] + 10;
	}
}

void Generare4(int a[], int &n)
{
	n = 5 + rand() % 5;
	for (int i = 0; i < n; i++) {
		a[i] = (i%2)?-1:i;
	}
}

void Fibo(int a[], int &n)
{
	n = 5 + rand() % 5;
	a[0] = a[1] = 1;
	for (int i = 2; i < n; i++) {
		a[i] = a[i - 2] + a[i - 1];
	}
}

bool EFibo(int f[], int L)
{
	if (f[0] != 1 || f[1] != 1) {
		return false;
	}
	for (int i = 2; i < L; i++) {
		if (f[i] != f[i - 2] + f[i - 1]) {
			return false;
		}
	}
	return true;
}

bool EDescrescator(int a[], int n)
{
	for (int i = 0; i < n - 1; i++) {
		if (a[i] < a[i + 1]) {
			return false;
		}
	}
	return true;
}

void InsertSort(int a[], int n)
{
	int i;
	for (int p = 1; p < n; p++) {
		int x = a[p];
		for (i = p-1; i>=0 && a[i]>x; i--) {
			a[i+1] = a[i];
		}
		a[i + 1] = x;
	}
}

void CountSort(int a[], int n, int m)
{
	int ct[101];
	for (int i = 0; i <= m; i++) {
		ct[i] = 0;
	}
	for (int i = 0; i < n; i++) {
		ct[a[i]]++;
	}

	n = 0;
	for (int i = 0; i <= m; i++) {
		for (int t = 1; t <= ct[i]; t++) {
			a[n++] = i;
		}
	}
}

void Interclasare(int a[], int n, int b[], int m, int c[], int &l)
{
	int i = 0, j = 0;
	l = 0;

	while (i < n && j < m) {
		if (a[i] < b[j]) {
			c[l++] = a[i++];
		}
		else {
			c[l++] = b[j++];
		}
	}
	while (i < n) {
		c[l++] = a[i++];
	}
	while (j < m) {
		c[l++] = b[j++];
	}
}

void GenerareRandomCrescator(int a[], int &n)
{
	n = 5 + rand() % 5;
	for (int i = 0; i < n; i++) {
		a[i] = i*10 + rand()%10;
	}
}
