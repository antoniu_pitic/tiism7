#pragma once

void Afisare(int[], int);
void Linie();

void GenerareRandomCrescator(int[], int&);
void GenerareCrescator(int[], int&);
void GenereazaRandom(int[], int&);

void GenerareRandomCrescatorRecursiv(int[], int&);
void GenerareCrescatorRecursiv(int[], int&);

void Generare4(int[], int&);

void Fibo(int[], int&);

bool EFibo(int[], int);
bool EDescrescator(int[], int);

void InsertSort(int[], int);
void CountSort(int[], int, int);

void Interclasare(	int a[],int n, 
					int b[],int m, 
					int c[],int &l);
